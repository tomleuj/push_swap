/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mediane.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 20:32:25 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/18 10:50:34 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		*ft_get_tab(t_stack *stack, int size)
{
	int		i;
	int		min;
	int		previous_min;
	int		*tab;
	t_stack	*tmp;

	i = 0;
	previous_min = INT_MIN;
	if (!(tab = (int *)malloc(sizeof(*tab) * size)))
		return (NULL);
	while (i < size)
	{
		tmp = stack;
		min = INT_MAX;
		while (tmp)
		{
			if (tmp->nb < min && tmp->nb > previous_min)
				min = tmp->nb;
			tmp = tmp->next;
		}
		previous_min = min;
		tab[i] = min;
		i++;
	}
	return (tab);
}

int				ft_get_med(t_stack *stack, t_opti *opti)
{
	int		med;
	int		size;
	int		*tab;
	t_stack	*tmp;

	med = 0;
	size = 0;
	tmp = stack;
	while (tmp)
	{
		size++;
		tmp = tmp->next;
	}
	tab = ft_get_tab(stack, size);
	med = tab[size / opti->n];
	if (size == 2)
		med = tab[0];
	free(tab);
	return (med);
}
