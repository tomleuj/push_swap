/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bbl_sort.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 16:22:15 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/17 16:26:23 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	ft_finish(t_stack *top_a)
{
	if (top_a->next)
	{
		while (top_a->next)
		{
			if (top_a->next->nb < top_a->nb)
				return (0);
			top_a = top_a->next;
		}
	}
	return (1);
}

static int	ft_get_max(t_stack *top)
{
	int max;

	max = top->nb;
	while (top)
	{
		if (top->nb > max)
			max = top->nb;
		top = top->next;
	}
	return (max);
}

void		ft_bbl_srt(t_stack **top_a, t_stack **top_b, t_opti *opti)
{
	int max;

	max = ft_get_max(*top_a);
	while (!ft_finish(*top_a))
	{
		if ((*top_a)->next->nb < (*top_a)->nb && (*top_a)->nb != max)
			ft_manip(SA, top_a, top_b, opti);
		if (!ft_finish(*top_a))
			ft_manip(RA, top_a, top_b, opti);
	}
}
