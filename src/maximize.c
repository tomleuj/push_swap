/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   maximize.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 17:43:33 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/17 20:39:42 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		*ft_tab_ref(t_stack *stack, int size)
{
	int *tab;
	int *tab_bis;

	tab = NULL;
	if (!(tab = (int *)malloc(sizeof(*tab) * size)))
		return (NULL);
	tab_bis = tab;
	while (stack)
	{
		*tab_bis = stack->nb;
		stack = stack->next;
		tab_bis++;
	}
	return (tab);
}

static t_stack	*ft_lst_reset(t_stack **stack, int *tab)
{
	t_stack	*tmp;

	tmp = *stack;
	while (*stack)
	{
		(*stack)->nb = *tab;
		*stack = (*stack)->next;
		tab++;
	}
	return (tmp);
}

void			ft_maximize(t_stack **top_a, t_stack **top_b, t_opti *opti)
{
	int *tab;

	tab = ft_tab_ref(*top_a, opti->size_a);
	opti->flag = 0;
	while (opti->n < opti->n_max)
	{
		opti->count = 0;
		ft_big_srt(top_a, top_b, opti);
		if (opti->count < opti->res)
		{
			opti->res = opti->count;
			opti->n_opti = opti->n;
		}
		*top_a = ft_lst_reset(top_a, tab);
		opti->n++;
	}
	free(tab);
	opti->flag = 1;
	opti->n = opti->n_opti;
	ft_big_srt(top_a, top_b, opti);
}
