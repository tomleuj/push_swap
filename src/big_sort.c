/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   big_sort.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/18 11:00:21 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/18 11:00:26 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		ft_finish(t_stack *top_a)
{
	while (top_a->next)
	{
		if (top_a->next->nb < top_a->nb)
			return (0);
		top_a = top_a->next;
	}
	return (1);
}

static int		ft_get_max(t_stack *top)
{
	int max;

	max = top->nb;
	while (top)
	{
		if (top->nb > max)
			max = top->nb;
		top = top->next;
	}
	return (max);
}

static void		ft_clear_stack(t_stack **top_a, t_stack **top_b, t_opti *opti)
{
	int max;

	max = ft_get_max(*top_b);
	while (*top_b)
	{
		if (ft_get_drc_back(*top_b, max))
		{
			if ((*top_b)->nb == max)
				ft_manip(PA, top_a, top_b, opti);
			else
				ft_manip(RB, top_a, top_b, opti);
		}
		else
		{
			if ((*top_b)->nb == max)
				ft_manip(PA, top_a, top_b, opti);
			else
				ft_manip(RRB, top_a, top_b, opti);
		}
		if (*top_b)
			max = ft_get_max(*top_b);
	}
}

static void		ft_quick_srt(t_stack **top_a, t_stack **top_b, t_opti *opti)
{
	int		med;
	t_stack *pivot;

	while ((*top_a)->next)
	{
		pivot = *top_a;
		med = ft_get_med(*top_a, opti);
		while (pivot->next)
			pivot = pivot->next;
		while (*top_a != pivot)
		{
			if ((*top_a)->nb > med)
				ft_manip(RA, top_a, top_b, opti);
			else if ((*top_a)->nb <= med)
				ft_manip(PB, top_a, top_b, opti);
		}
	}
}

void			ft_big_srt(t_stack **top_a, t_stack **top_b, t_opti *opti)
{
	if (!ft_finish(*top_a))
	{
		ft_quick_srt(top_a, top_b, opti);
		ft_clear_stack(top_a, top_b, opti);
	}
}
