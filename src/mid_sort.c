/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mid_sort.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/18 11:00:06 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/18 11:00:09 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		ft_finish(t_stack *top_a, t_stack *top_b)
{
	if (top_b)
		return (0);
	if (top_a->next)
	{
		while (top_a->next)
		{
			if (top_a->next->nb < top_a->nb)
				return (0);
			top_a = top_a->next;
		}
	}
	return (1);
}

static int		ft_get_minus(t_stack *top)
{
	int min;

	min = top->nb;
	while (top)
	{
		if (top->nb < min)
			min = top->nb;
		top = top->next;
	}
	return (min);
}

static void		ft_go_last(t_stack **top_a, t_stack **top_b, t_opti *opti)
{
	int min;
	int	direction;

	min = ft_get_minus(*top_a);
	direction = ft_get_drc_back(*top_a, min);
	while ((*top_a)->nb != min)
	{
		if (direction > 0)
			ft_manip(RA, top_a, top_b, opti);
		else if (direction <= 0)
			ft_manip(RRA, top_a, top_b, opti);
	}
	ft_manip(PB, top_a, top_b, opti);
}

void			ft_mid_srt(t_stack **top_a, t_stack **top_b, t_opti *opti)
{
	if (!(ft_finish(*top_a, *top_b)))
	{
		opti->size_a = ft_get_size(*top_a);
		while (opti->size_a > 3)
		{
			ft_go_last(top_a, top_b, opti);
			opti->size_a = ft_get_size(*top_a);
		}
		ft_bbl_srt(top_a, top_b, opti);
		while (*top_b)
			ft_manip(PA, top_a, top_b, opti);
	}
}
