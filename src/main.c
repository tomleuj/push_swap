/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 16:39:16 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/18 14:55:37 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		ft_is_double(t_stack *stack)
{
	int		nb;
	int		flag;
	t_stack	*tmp;
	t_stack	*start;

	nb = 0;
	flag = 0;
	start = stack;
	while (stack)
	{
		flag = 0;
		tmp = start;
		nb = stack->nb;
		while (tmp)
		{
			if (nb == tmp->nb)
				flag++;
			if (flag == 2)
				return (0);
			tmp = tmp->next;
		}
		stack = stack->next;
	}
	return (1);
}

static void		ft_eraser(t_stack **top_a, t_stack **top_b)
{
	t_stack	*tmp;

	tmp = *top_a;
	while (*top_a)
	{
		tmp = tmp->next;
		free(*top_a);
		*top_a = tmp;
	}
	tmp = *top_b;
	while (*top_b)
	{
		tmp = tmp->next;
		free(*top_b);
		*top_b = tmp;
	}
}

static void		ft_set_opti(t_opti *opti)
{
	opti->n = 2;
	opti->res = 2147483647;
	opti->flag = 1;
	opti->n_max = 20;
	opti->count = 0;
	opti->n_opti = 0;
	opti->size_a = 0;
}

static void		ft_what_kind_of_sort(t_stack **top_a, t_stack **top_b)
{
	int		*tab;
	t_opti	opti;

	tab = NULL;
	ft_set_opti(&opti);
	opti.size_a = ft_get_size(*top_a);
	if (opti.size_a <= 3)
		ft_bbl_srt(top_a, top_b, &opti);
	else if (opti.size_a <= 5 && opti.size_a > 3)
		ft_mid_srt(top_a, top_b, &opti);
	else
		ft_maximize(top_a, top_b, &opti);
}

int				main(int ac, char **av)
{
	int		count;
	t_stack	*top_a;
	t_stack	*top_b;

	count = 0;
	top_a = NULL;
	top_b = NULL;
	if (!av[1])
		return (1);
	while (++count < ac && count > 0)
		top_a = ft_sncf(av[count], top_a, &count);
	if (top_a == NULL || !ft_is_double(top_a))
	{
		ft_putendl("push_swap: error");
		return (1);
	}
	ft_what_kind_of_sort(&top_a, &top_b);
	ft_eraser(&top_a, &top_b);
	return (0);
}
