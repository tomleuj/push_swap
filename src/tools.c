/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/15 18:32:15 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/18 10:38:05 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_print_moves(int rules)
{
	rules == PA ? ft_putendl("pa") : NULL;
	rules == PB ? ft_putendl("pb") : NULL;
	rules == SA ? ft_putendl("sa") : NULL;
	rules == SB ? ft_putendl("sb") : NULL;
	rules == SS ? ft_putendl("ss") : NULL;
	rules == RA ? ft_putendl("ra") : NULL;
	rules == RB ? ft_putendl("rb") : NULL;
	rules == RR ? ft_putendl("rr") : NULL;
	rules == RRA ? ft_putendl("rra") : NULL;
	rules == RRB ? ft_putendl("rrb") : NULL;
	rules == RRR ? ft_putendl("rrr") : NULL;
}

int		ft_get_size(t_stack *stack)
{
	int size;

	size = 0;
	if (stack)
	{
		while (stack)
		{
			stack = stack->next;
			size++;
		}
	}
	return (size);
}
