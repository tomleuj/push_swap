/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 14:31:26 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/18 10:53:50 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	ft_push(t_stack **top_x, t_stack **top_y)
{
	t_stack *tmp;

	if (*top_x)
	{
		tmp = *top_y;
		*top_y = *top_x;
		*top_x = (*top_x)->next;
		(*top_y)->next = tmp;
	}
}

static void	ft_swaps(t_stack **top, t_stack **unused)
{
	t_stack *tmp;

	unused += 0;
	if (*top && (*top)->next)
	{
		tmp = (*top)->next;
		(*top)->next = tmp->next;
		tmp->next = *top;
		*top = tmp;
	}
}

static void	ft_back_rotate(t_stack **top, t_stack **unused)
{
	t_stack *tmp;

	unused += 0;
	if (*top && (*top)->next)
	{
		tmp = *top;
		while (tmp->next && tmp->next->next)
			tmp = tmp->next;
		tmp->next->next = *top;
		*top = tmp->next;
		tmp->next = NULL;
	}
}

static void	ft_front_rotate(t_stack **top, t_stack **unused)
{
	t_stack *tmp;
	t_stack *end;

	unused += 0;
	if (*top && (*top)->next)
	{
		tmp = *top;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = *top;
		end = *top;
		*top = (*top)->next;
		end->next = NULL;
	}
}

void		ft_manip(int rules, t_stack **top_a,
		t_stack **top_b, t_opti *opti)
{
	void	(*tab[4])(t_stack **, t_stack **);

	tab[0] = &ft_push;
	tab[1] = &ft_swaps;
	tab[2] = &ft_front_rotate;
	tab[3] = &ft_back_rotate;
	if (rules == SS || rules == RR || rules == RRR ||
			rules == PB || rules == SA || rules == RA || rules == RRA)
	{
		(*tab[rules / 3])(top_a, top_b);
		opti->count++;
	}
	if (rules == SS || rules == RR || rules == RRR ||
			rules == PA || rules == SB || rules == RB || rules == RRB)
	{
		(*tab[rules / 3])(top_b, top_a);
		if (rules != SS && rules != RR && rules != RRR)
			opti->count++;
	}
	if (opti->flag == 1)
		ft_print_moves(rules);
}
