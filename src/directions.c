/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   directions.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/18 11:00:49 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/18 11:00:52 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		ft_get_dst_back(t_stack *top, int max)
{
	int dst;

	dst = 0;
	while (top->nb != max)
	{
		top = top->next;
		dst++;
	}
	return (dst);
}

int				ft_get_drc_back(t_stack *top, int max)
{
	int			dst;
	t_stack		*tmp;

	dst = 0;
	tmp = top;
	while (tmp)
	{
		tmp = tmp->next;
		dst++;
	}
	if (ft_get_dst_back(top, max) <= (dst / 2))
		return (1);
	return (0);
}
