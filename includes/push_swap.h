/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 16:27:13 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/18 11:25:51 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H
# define TRUE 2
# define END 1
# define FALSE 0
# define PA 1
# define PB 2
# define SA 3
# define SB 4
# define SS 5
# define RA 6
# define RB 7
# define RR 8
# define RRA 9
# define RRB 10
# define RRR 11

# include "../libft/libft.h"
# include <limits.h>

typedef struct		s_stack
{
	int				nb;
	struct s_stack	*next;
}					t_stack;

typedef struct		s_opti
{
	int				n;
	int				res;
	int				flag;
	int				n_max;
	int				count;
	int				n_opti;
	int				size_a;
}					t_opti;

t_stack				*ft_sncf(char *av, t_stack *stack, int *count);
void				ft_print_moves(int rules);
void				ft_manip(int rules, t_stack **top_a,
										t_stack **top_b, t_opti *opti);

void				ft_bbl_srt(t_stack **top_a, t_stack **top_b, t_opti *opti);
void				ft_mid_srt(t_stack **top_a, t_stack **top_b, t_opti *opti);
void				ft_big_srt(t_stack **top_a, t_stack **top_b, t_opti *opti);
void				ft_maximize(t_stack **top_a, t_stack **top_b, t_opti *opti);

int					ft_get_med(t_stack *stack, t_opti *opti);
int					ft_get_drc_back(t_stack *top, int max);
int					ft_get_size(t_stack *stack);

#endif
