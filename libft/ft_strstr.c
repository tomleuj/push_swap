/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 22:35:49 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/11 23:16:00 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *lil)
{
	int i;
	int j;

	i = 0;
	j = 0;
	if (!lil[j])
		return ((char *)big);
	while (big[i])
	{
		while (lil[j] == big[i + j])
		{
			if (!lil[++j])
				return (char *)(big + i);
		}
		j = 0;
		i++;
	}
	return (NULL);
}
