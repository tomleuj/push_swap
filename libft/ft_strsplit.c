/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/12 10:42:26 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/15 17:08:00 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_line(const char *s, char c)
{
	int i;
	int blocks;
	int green_ligth;

	if (!s)
		return (-1);
	i = 0;
	blocks = 0;
	green_ligth = 1;
	while (s[i])
	{
		if (s[i] != c && green_ligth == 1)
		{
			green_ligth = 0;
			blocks++;
		}
		else if (s[i] == c)
			green_ligth = 1;
		i++;
	}
	return (blocks);
}

static int	ft_block(const char *s, char c, int i)
{
	int size;

	size = 0;
	while (s[i] != c && s[i])
	{
		size++;
		i++;
	}
	return (size);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**cut;
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	if (!(cut = (char**)malloc(sizeof(char*) * ft_line(s, c) + 1)))
		return (NULL);
	while (s[i])
	{
		while (s[i] == c && s[i])
			i++;
		if (s[i])
		{
			k = 0;
			if (!(cut[j] = (char*)malloc(sizeof(char) * ft_block(s, i, c) + 1)))
				return (NULL);
			while (s[i] != c && s[i])
				cut[j][k++] = s[i++];
			cut[j++][k] = '\0';
		}
	}
	cut[j] = NULL;
	return (cut);
}
