/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/12 05:14:15 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/15 15:44:33 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char *grume;

	if (!s)
		return (NULL);
	if (!(grume = ft_strnew(len)))
		return (NULL);
	while (start--)
		s++;
	return (ft_strncpy(grume, s, len));
}
