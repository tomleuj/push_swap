/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 19:50:43 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/11 22:16:08 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t n)
{
	size_t nret;
	size_t ndst;
	size_t nsrc;

	nret = ft_strlen(dst);
	ndst = ft_strlen(dst);
	nsrc = ft_strlen(src);
	if (n < ndst)
		return (n + nsrc);
	while ((ndst + 1) < n && *src)
		*(dst + (ndst++)) = *src++;
	*(dst + ndst) = '\0';
	return (nret + nsrc);
}
