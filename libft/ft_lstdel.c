/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/12 13:34:56 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/12 16:55:53 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	while (*(alst) != NULL)
	{
		(*del)((*(alst))->content, (*(alst))->content_size);
		free(*(alst));
		(*(alst)) = (*(alst))->next;
	}
	*alst = NULL;
}
