/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/12 02:01:48 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/14 14:40:51 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(const char *s, char (*f)(char))
{
	char *str;
	char *ret;

	if (!s || !(*f))
		return (NULL);
	if (!(str = ft_strnew(ft_strlen(s))))
		return (NULL);
	ret = str;
	while (*s)
		*str++ = (*f)(*s++);
	*str = '\0';
	return (ret);
}
