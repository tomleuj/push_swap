/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/12 02:31:16 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/14 18:28:33 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int		i;
	char	*str;
	char	*ret;

	i = 0;
	if (!s || !(*f))
		return (NULL);
	if (!(str = ft_strnew(ft_strlen(s))))
		return (NULL);
	ret = str;
	while (*s)
		*str++ = (*f)(i++, *s++);
	*str = '\0';
	return (ret);
}
