/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/09 19:25:30 by tlejeune          #+#    #+#             */
/*   Updated: 2017/01/17 09:34:34 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_size(int *n, int *ng, int base)
{
	size_t	size;
	int		tmp;

	size = 0;
	if (*n < 0)
	{
		if (base == 10)
		{
			*ng = 1;
			size++;
		}
		*n *= -1;
	}
	tmp = *n;
	while (tmp /= base)
		size++;
	return (++size);
}

char		*ft_itoa_base(int n, int base)
{
	size_t	size;
	char	*set;
	char	*s;
	int		ng;

	ng = 0;
	set = "0123456789abcdef";
	if (base <= 0 || base > 16)
		return (NULL);
	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	size = ft_size(&n, &ng, base);
	if (!(s = ft_strnew(size)))
		return (NULL);
	while (size--)
	{
		s[size] = set[n % base];
		n /= base;
	}
	if (ng)
		s[size] = '-';
	return (s);
}
