/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/12 07:23:50 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/14 17:05:43 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_size(int *n, int *ng)
{
	size_t	size;
	int		tmp;

	size = 0;
	tmp = *n;
	if (tmp < 0)
	{
		tmp *= -1;
		*n *= -1;
		*ng = 1;
		size++;
	}
	while (tmp > 9)
	{
		tmp /= 10;
		size++;
	}
	return (++size);
}

char		*ft_itoa(int n)
{
	size_t	size;
	char	*s;
	int		ng;

	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	ng = 0;
	size = ft_size(&n, &ng);
	if (!(s = (char *)malloc(sizeof(*s) * size + 1)))
		return (NULL);
	s[size] = '\0';
	while (size--)
	{
		s[size] = (n % 10 + '0');
		n /= 10;
	}
	if (ng == 1)
		*s = '-';
	return (s);
}
