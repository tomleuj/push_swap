# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/02 16:33:17 by tlejeune          #+#    #+#              #
#    Updated: 2017/02/18 10:59:52 by tlejeune         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = push_swap
CC = gcc
FLAGS = -Wall -Wextra -Werror
DEBUG = yes

ifeq ($(DEBUG),yes)
	FLAGS += -g
endif

TRASH = info .DS_Store push_swap.dSYM checker.dSYM

SRC_PATH = ./src/
INCLUDES_PATH = ./includes/
OBJ_PATH = ./obj/

LIBFT_PATH = ./libft/
LIBFT_H = -I$(LIBFT_PATH)
LIBFT_A = $(LIBFT_PATH)libft.a
LIBFT_LINK = -L$(LIBFT_PATH) -lft

CHECKER = ./checker
CHECKER_PATH = ./check_dir/

SRC = main.c\
	  stacks.c\
	  instructions.c\
	  bbl_sort.c\
	  mid_sort.c\
	  big_sort.c\
	  mediane.c\
	  maximize.c\
	  directions.c\
	  tools.c\

OBJ	= $(addprefix $(OBJ_PATH),$(SRC:.c=.o))

all: $(NAME)

$(NAME): obj $(LIBFT_A) $(CHECKER) $(OBJ)
	$(CC) $(FLAGS) -o $@ $(LIBFT_A) $(OBJ)

obj:
	mkdir $(OBJ_PATH)

$(LIBFT_A):
	make -C $(LIBFT_PATH)

$(CHECKER):
	make -C $(CHECKER_PATH)

$(OBJ_PATH)%.o:$(SRC_PATH)%.c
	$(CC) $(FLAGS) $(LIBFT_H) -I$(INCLUDES_PATH) -o $@ -c $<

clean:
	rm -rf $(OBJ_PATH)
	rm -rf $(TRASH)
	make clean -C $(LIBFT_PATH)
	make clean -C $(CHECKER_PATH)

fclean: clean
	rm -rf $(NAME)
	rm -rf $(CHECKER)
	make fclean -C $(LIBFT_PATH)
	make fclean -C $(CHECKER_PATH)

re: fclean all
