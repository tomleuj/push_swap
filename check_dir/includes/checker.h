/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/12 14:36:43 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/23 17:26:20 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_H
# define CHECKER_H
# define TRUE 2
# define END 1
# define FALSE 0
# define PA 1
# define PB 2
# define SA 3
# define SB 4
# define SS 5
# define RA 6
# define RB 7
# define RR 8
# define RRA 9
# define RRB 10
# define RRR 11

# include "../libft/libft.h"

typedef struct			s_stack_c
{
	int					nb;
	struct s_stack_c	*next;
}						t_stack_c;

typedef struct			s_box_c
{
	int					manip;
	struct s_box_c		*next;
}						t_box_c;

typedef struct			s_flags_c
{
	int					count;
	int					stacks;
	int					instructions;
}						t_flags_c;

t_stack_c				*ft_sncf(char *av, t_stack_c *stack, int *i);
t_box_c					*ft_set_box(t_box_c *box);

int						ft_retry_bitch(char **av);
void					ft_stacks_eraser(t_stack_c **top_a, t_stack_c **top_b);
void					ft_box_eraser(t_box_c **box);
void					ft_print_res(t_flags_c flags, t_stack_c *top_a,
													t_stack_c *top_b);

void					ft_push(t_stack_c **top_x, t_stack_c **top_y);
void					ft_swaps(t_stack_c **top, t_stack_c **unused);
void					ft_front_rotate(t_stack_c **top, t_stack_c **unused);
void					ft_back_rotate(t_stack_c **top, t_stack_c **unused);
void					ft_manip(int rules, t_stack_c **top_a,
										t_stack_c **top_b, t_flags_c *flags);

#endif
