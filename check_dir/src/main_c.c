/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_c.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/12 14:06:10 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/23 17:28:25 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static int		ft_is_double(t_stack_c *stack)
{
	int			nb;
	int			flag;
	t_stack_c	*tmp;
	t_stack_c	*start;

	nb = 0;
	flag = 0;
	start = stack;
	while (stack)
	{
		flag = 0;
		tmp = start;
		nb = stack->nb;
		while (tmp)
		{
			if (nb == tmp->nb)
				flag++;
			if (flag == 2)
				return (0);
			tmp = tmp->next;
		}
		stack = stack->next;
	}
	return (1);
}

static void		ft_sort(t_stack_c **top_a, t_stack_c **top_b, t_box_c *sort,
		t_flags_c *flags)
{
	t_box_c *tmp;

	sort = ft_set_box(sort);
	tmp = sort;
	flags->count = 0;
	while (sort)
	{
		ft_manip(sort->manip, top_a, top_b, flags);
		sort = sort->next;
	}
	ft_box_eraser(&tmp);
}

static void		ft_set_flags(char **av, int *i, t_flags_c *flags)
{
	flags->count = 0;
	flags->stacks = 0;
	flags->instructions = 0;
	if (av[1])
	{
		if (ft_strcmp(av[1], "-i") == 0 || ft_strcmp(av[1], "-s") == 0 ||
			ft_strcmp(av[1], "-si") == 0 || ft_strcmp(av[1], "-is") == 0)
		{
			(*i)++;
			if (ft_strcmp(av[1], "-s") == 0 || ft_strcmp(av[1], "-si") == 0 ||
				ft_strcmp(av[1], "-is") == 0)
				flags->stacks = 1;
			if (ft_strcmp(av[1], "-i") == 0 || ft_strcmp(av[1], "-si") == 0 ||
				ft_strcmp(av[1], "-is") == 0)
				flags->instructions = 1;
		}
	}
}

int				main(int ac, char **av)
{
	int			i;
	t_box_c		*sort;
	t_flags_c	flags;
	t_stack_c	*top_a;
	t_stack_c	*top_b;

	i = 0;
	sort = NULL;
	top_a = NULL;
	top_b = NULL;
	ft_set_flags(av, &i, &flags);
	if (!ft_retry_bitch(av))
		return (1);
	while (++i < ac && i > 0)
		top_a = ft_sncf(av[i], top_a, &i);
	if (top_a == NULL || !ft_is_double(top_a))
	{
		ft_putendl("Error");
		return (1);
	}
	ft_sort(&top_a, &top_b, sort, &flags);
	ft_print_res(flags, top_a, top_b);
	ft_stacks_eraser(&top_a, &top_b);
	return (0);
}
