/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   box_c.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 12:33:29 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/18 16:18:22 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static int		ft_manip_converter(char *line, t_box_c *box)
{
	if (ft_strcmp(line, "pa") == 0)
		box->manip = PA;
	else if (ft_strcmp(line, "pb") == 0)
		box->manip = PB;
	else if (ft_strcmp(line, "ra") == 0)
		box->manip = RA;
	else if (ft_strcmp(line, "rb") == 0)
		box->manip = RB;
	else if (ft_strcmp(line, "rr") == 0)
		box->manip = RR;
	else if (ft_strcmp(line, "rra") == 0)
		box->manip = RRA;
	else if (ft_strcmp(line, "rrb") == 0)
		box->manip = RRB;
	else if (ft_strcmp(line, "rrr") == 0)
		box->manip = RRR;
	else if (ft_strcmp(line, "sa") == 0)
		box->manip = SA;
	else if (ft_strcmp(line, "sb") == 0)
		box->manip = SB;
	else if (ft_strcmp(line, "ss") == 0)
		box->manip = SS;
	return (box->manip);
}

static t_box_c	*ft_ret(t_box_c *box, t_box_c *tmp)
{
	if (!box->next)
	{
		free(box);
		return (NULL);
	}
	tmp = box;
	box = box->next;
	free(tmp);
	return (box);
}

t_box_c			*ft_set_box(t_box_c *box)
{
	char	*line;
	t_box_c *tmp;

	line = NULL;
	if (!(tmp = (t_box_c *)malloc(sizeof(*box))))
		return (NULL);
	tmp->next = NULL;
	box = tmp;
	while (get_next_line(0, &line))
	{
		if (!(tmp->next = (t_box_c *)malloc(sizeof(*tmp))))
			return (NULL);
		tmp = tmp->next;
		tmp->manip = 0;
		tmp->next = NULL;
		if (!(ft_manip_converter(line, tmp)))
		{
			free(line);
			ft_box_eraser(&box);
			ft_putendl("Error");
			exit(1);
		}
		free(line);
	}
	return (ft_ret(box, tmp));
}
