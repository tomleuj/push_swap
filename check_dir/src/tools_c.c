/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_c.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/18 14:55:31 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/23 17:39:44 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void			ft_stacks_eraser(t_stack_c **top_a, t_stack_c **top_b)
{
	t_stack_c	*tmp_bis;

	tmp_bis = *top_a;
	while (*top_a)
	{
		tmp_bis = tmp_bis->next;
		free(*top_a);
		*top_a = tmp_bis;
	}
	tmp_bis = *top_b;
	while (*top_b)
	{
		tmp_bis = tmp_bis->next;
		free(*top_b);
		*top_b = tmp_bis;
	}
}

void			ft_box_eraser(t_box_c **box)
{
	t_box_c		*tmp;

	tmp = *box;
	while (*box)
	{
		tmp = tmp->next;
		free(*box);
		*box = tmp;
	}
}

int				ft_retry_bitch(char **av)
{
	if (!av[1] ||
	((ft_strcmp(av[1], "-i") == 0 || ft_strcmp(av[1], "-s") == 0 ||
	ft_strcmp(av[1], "-si") == 0 || ft_strcmp(av[1], "-is") == 0) &&
	!av[2]))
		return (0);
	else
		return (1);
}

static int		ft_finish(t_stack_c *top_a, t_stack_c *top_b)
{
	if (top_b || !top_a)
		return (0);
	if (top_a && top_a->next)
	{
		while (top_a->next)
		{
			if (top_a->next->nb < top_a->nb)
				return (0);
			top_a = top_a->next;
		}
	}
	return (1);
}

void			ft_print_res(t_flags_c flags, t_stack_c *top_a,
		t_stack_c *top_b)
{
	ft_putstr("\033[32m");
	ft_finish(top_a, top_b) ? ft_putendl("OK") : ft_putendl("KO");
	ft_putstr("Nombre d'instructions effectuees: ");
	ft_putnbr(flags.count);
	ft_putstr("\033[0m");
	ft_putchar('\n');
}
