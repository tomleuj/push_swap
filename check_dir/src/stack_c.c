/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_c.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/12 14:35:58 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/23 16:21:35 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static int			ft_ratp(char **av, int *nb)
{
	char *tmp;

	if (**av)
	{
		while (**av == ' ')
			(*av)++;
		*nb = ft_atoi(*av);
		tmp = ft_itoa(*nb);
		if (ft_strncmp(tmp, *av, ft_strlen(tmp) != 0))
		{
			free(tmp);
			return (FALSE);
		}
		free(tmp);
		if (**av == '-')
			(*av)++;
		while (ft_isdigit(**av))
			(*av)++;
		if (**av == '-')
			return (FALSE);
		return (TRUE);
	}
	return (END);
}

static t_stack_c	*ft_go_ahead(t_stack_c *stack)
{
	if (stack)
	{
		while (stack->next)
			stack = stack->next;
	}
	return (stack);
}

static void			ft_quick_erase(t_stack_c **stack)
{
	t_stack_c *tmp;

	while (*stack)
	{
		tmp = (*stack)->next;
		free(*stack);
		(*stack) = tmp;
	}
}

static t_stack_c	*ft_ret(int flag, int *count,
					t_stack_c *elem, t_stack_c *stack)
{
	if (flag == FALSE)
	{
		*count = -1;
		if (elem)
			elem->next = NULL;
		if (stack)
			ft_quick_erase(&stack);
		return (NULL);
	}
	return (stack);
}

t_stack_c			*ft_sncf(char *av, t_stack_c *stack, int *count)
{
	int			nb;
	int			flag;
	t_stack_c	*tmp;
	t_stack_c	*elem;

	nb = 0;
	tmp = NULL;
	elem = NULL;
	while ((flag = ft_ratp(&av, &nb)) == TRUE)
	{
		if (!(elem = (t_stack_c *)malloc(sizeof(*elem))))
			return (NULL);
		elem->nb = nb;
		elem->next = NULL;
		if (!stack)
			stack = elem;
		else
		{
			tmp = ft_go_ahead(stack);
			tmp->next = elem;
		}
	}
	return (ft_ret(flag, count, elem, stack));
}
