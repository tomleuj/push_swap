/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fct_p_c.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/12 15:31:58 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/23 17:10:53 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void		ft_print_moves(int rules)
{
	ft_putstr("\033[31m");
	rules == PA ? ft_putendl("-> pa") : NULL;
	rules == PB ? ft_putendl("-> pb") : NULL;
	rules == SA ? ft_putendl("-> sa") : NULL;
	rules == SB ? ft_putendl("-> sb") : NULL;
	rules == SS ? ft_putendl("-> ss") : NULL;
	rules == RA ? ft_putendl("-> ra") : NULL;
	rules == RB ? ft_putendl("-> rb") : NULL;
	rules == RR ? ft_putendl("-> rr") : NULL;
	rules == RRA ? ft_putendl("-> rra") : NULL;
	rules == RRB ? ft_putendl("-> rrb") : NULL;
	rules == RRR ? ft_putendl("-> rrr") : NULL;
	ft_putstr("\033[0m");
}

static void		ft_print_stacks(t_stack_c *top_a, t_stack_c *top_b)
{
	ft_putstr("\033[34m");
	ft_putendl("\nPile A:");
	while (top_a)
	{
		ft_putnbr(top_a->nb);
		ft_putstr(" | ");
		top_a = top_a->next;
	}
	ft_putchar('\n');
	ft_putstr("\033[36m");
	ft_putendl("\nPile B:");
	while (top_b)
	{
		ft_putnbr(top_b->nb);
		ft_putstr(" | ");
		top_b = top_b->next;
	}
	ft_putstr("\033[0m");
	ft_putstr("\n~~~~~\n\n");
}

void			ft_manip(int rules, t_stack_c **top_a,
		t_stack_c **top_b, t_flags_c *flags)
{
	void	(*tab[4])(t_stack_c **, t_stack_c **);

	tab[0] = &ft_push;
	tab[1] = &ft_swaps;
	tab[2] = &ft_front_rotate;
	tab[3] = &ft_back_rotate;
	if (rules == SS || rules == RR || rules == RRR ||
			rules == PB || rules == SA || rules == RA || rules == RRA)
	{
		(*tab[rules / 3])(top_a, top_b);
		flags->count++;
	}
	if (rules == SS || rules == RR || rules == RRR ||
			rules == PA || rules == SB || rules == RB || rules == RRB)
	{
		(*tab[rules / 3])(top_b, top_a);
		if (rules != SS && rules != RR && rules != RRR)
			flags->count++;
	}
	if (flags->instructions == 1)
		ft_print_moves(rules);
	if (flags->stacks == 1)
		ft_print_stacks(*top_a, *top_b);
}
