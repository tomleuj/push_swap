/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions_c.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/12 15:28:28 by tlejeune          #+#    #+#             */
/*   Updated: 2017/02/12 20:46:35 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void	ft_push(t_stack_c **top_x, t_stack_c **top_y)
{
	t_stack_c *tmp;

	if (*top_x)
	{
		tmp = *top_y;
		*top_y = *top_x;
		*top_x = (*top_x)->next;
		(*top_y)->next = tmp;
	}
}

void	ft_swaps(t_stack_c **top, t_stack_c **unused)
{
	t_stack_c *tmp;

	unused += 0;
	if (*top && (*top)->next)
	{
		tmp = (*top)->next;
		(*top)->next = tmp->next;
		tmp->next = *top;
		*top = tmp;
	}
}

void	ft_back_rotate(t_stack_c **top, t_stack_c **unused)
{
	t_stack_c *tmp;

	unused += 0;
	if (*top && (*top)->next)
	{
		tmp = *top;
		while (tmp->next && tmp->next->next)
			tmp = tmp->next;
		tmp->next->next = *top;
		*top = tmp->next;
		tmp->next = NULL;
	}
}

void	ft_front_rotate(t_stack_c **top, t_stack_c **unused)
{
	t_stack_c *tmp;
	t_stack_c *end;

	unused += 0;
	if (*top && (*top)->next)
	{
		tmp = *top;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = *top;
		end = *top;
		*top = (*top)->next;
		end->next = NULL;
	}
}
